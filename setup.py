#! /usr/bin/env python
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(
    name="movie_recommendations",
    version="0.1.0",
    description="Wantsome Project",
    long_description=open("README.md").read(),
    author="Wansome - Python Seria 2",
    url="https://gitlab.com/wantsome-projects/hunt-the-wumpus",
    packages=["movie_recommendations"],
    entry_points={
        'console_scripts': [
            'hunt-the-wumpus = movie_recommendations.cmdline:main',
        ],
    },
    requires=open("requirements.txt").readlines(),
)
