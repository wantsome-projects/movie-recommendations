"""Command line interface application for the current project."""

#!/usr/bin/env python3
# Import packages

""" 
This script allows you to search idifferent movies, actors, producers and directors based on the information available on IMDB and OMDBAPI

"""

import gzip
import itertools
import omdb
import os
import pandas as pd
import re
import urllib.request
from urllib.parse import urljoin

from pandas import ExcelWriter
#from pandas import ExcelFile
#import numpy as np





def read_imdbfiles(baseURL, download_folder, endURL):
    """ 
    Function to read IMDB files
    """
    response = urllib.request.urlopen(urljoin(baseURL,endURL))
    outfile = endURL[:-3]
    with open(os.path.join(os.getcwd(),download_folder,outfile), 'wb') as f:
        f.write(gzip.decompress(response.read()))

def print_results1(movies):
    """ 
    Function to display the search results
    """
    print(movies['title'])
    print(" ----- Year: %s" %movies['year'])
    print(" ----- Duration: %s" %movies['runtime'])
    print(" ----- Genre: %s" %movies['genre'])
    print(" ----- Actors: %s "%movies['actors'])
    print(" ----- Plot: %s" %movies['plot'])
    print(" ----- Poster: %s" %movies['poster'])
    print(" ----- Rating IMDB: %s" %movies['imdb_rating'])
    print()

def print_results2(film, item):
    """ 
    Function to display the search results
    """
    print((film['primaryTitle'][film['tconst'] == item]).to_string(index=False))
    print(" ----- Year: %s" %(film['startYear'][film['tconst'] == item]).to_string(index=False))
    print(" ----- Duration: %s" %(film['runtimeMinutes'][film['tconst'] == item]).to_string(index=False))
    print(" ----- Genre: %s" %(film['genres'][film['tconst'] == item]).to_string(index=False))
    print(" ----- Rating IMDB: %s" %(film['averageRating'][film['tconst'] == item]).to_string(index=False))
    print()

def display_search_results(id_list):
    """ 
    Function to display all movies an actor/actress played
    """
    movies = {}
    for item in itertools.chain.from_iterable(id_list):
        movies = omdb.imdbid(item)
        print_results1(movies)


def search_for(person_to_search_raw, path, filename, searchfor = []):
            
    person_to_search = person_to_search_raw.title()
    if not os.path.isfile(path):
        read_imdbfiles(baseURL, download_folder, endURL = filename)
    #din name basics citesc actorul (sau altceva) si knownForTitle si iau toate id-urile si le trec in omdb.get
    myfile = path
    all_names = pd.read_csv(myfile,sep='\t')
    #print(all_names.head()) 

    searchfor = list(searchfor)
    id_films = (all_names.loc[(all_names['primaryProfession'].str.contains('|'.join(searchfor))) & (all_names['primaryName'].str.contains(person_to_search))])
            
    if id_films.empty == True:
        print("The name of the %s can't be found!" %searchfor)
        return


    id_list = []
    for row in id_films.itertuples(): 
        id_list.append(getattr(row, 'knownForTitles')) 


    id_list = [i.split(',') for i in id_list] 

    display_search_results(id_list)






def display_movie_based_search(movie):
    """ 
    Function to display information about a movie 
    """
    id_list = []
    for valoare in omdb.search_movie(movie):
        for key in valoare:
            if key == 'imdb_id':
                id_list.append(valoare[key])
    
    movies = {}
    for item in id_list:
        movies = omdb.imdbid(item)
        print_results1(movies)
  


def display_year_based_search(film, top):
    """ 
    Function to display x movies from an year ordered by their rating

    """
    id_list = []

    for row in film.itertuples(): 
        if len(id_list) < int(top):
            id_list.append(getattr(row, 'tconst')) 


    movies = {}
    for item in id_list:
        movies = omdb.imdbid(item)
        if movies:
            print_results1(movies)
        else:
            print_results2(film, item)



def display_year_based_search_all(film, download_folder):
    """ 
    Function to create an excel file with all movies from an year

    """
    writer = ExcelWriter(os.path.join(os.getcwd(), download_folder, 'movies.xlsx'))
    film.to_excel(writer,'Sheet1', index=False)
    writer.save()

    print("You can open the file from here %r" % os.path.join(os.getcwd(), download_folder, 'movies.xlsx'))




def main():


    omdb.set_default('apikey', '39c1297a')
    omdb.set_default('tomatoes', False)

    baseURL = "https://datasets.imdbws.com/"
    download_folder = 'Movies'

    if not os.path.exists(os.path.join(os.getcwd(), download_folder)):
        os.mkdir(download_folder)



    print("-----------")
    print("Welcome!!!")
    print("-----------")
    print()
    print("Here you can get the information needed to help you chose a movie based on several criteria of your choice.")
    print('Press help for available commands.')
    while True:
        command = input('$ ')
        if command.lower() == 'help':
            print('movie - if you want to search information about a specific movie')
            print('actor - if you want to visualize information about all movies an actor/ actress was involved')
            print('director - if you want to visualize information about all movies directed by someone')
            print('producer - if you want to visualize information about all movies produced by someone')
            print('year - if you want to visualize information about all movies released in a specific year')
            print('exit - if you want to exit')
        elif command.lower() == 'exit':
            print("You closed the application! See you next time!")
            break
        elif command.lower() == 'movie':
            movie = input("Name of the movie you are searching for: ")
            #print(omdb.search_movie(movie))
            
            display_movie_based_search(movie)
        elif command.lower() == 'actor':
            person_to_search_raw = input("Name of the actor/ actress you are searching for: ")
            search_for(path = os.path.join(os.getcwd(), download_folder, "name.basics.tsv"), filename = "name.basics.tsv.gz", searchfor = ['actor', 'actress'], person_to_search_raw = person_to_search_raw)
        elif command.lower() == 'director':
            person_to_search_raw = input("Name of the director you are searching for: ")
            search_for(path = os.path.join(os.getcwd(), download_folder, "name.basics.tsv"), filename = "name.basics.tsv.gz", searchfor = ['director'], person_to_search_raw = person_to_search_raw)
        elif command.lower() == 'producer':
            person_to_search_raw = input("Name of the producer you are searching for: ")
            search_for(path = os.path.join(os.getcwd(), download_folder, "name.basics.tsv"), filename = "name.basics.tsv.gz", searchfor = ['producer'], person_to_search_raw = person_to_search_raw)

        elif command.lower() == 'year':
            year_raw = input("Type the year you want to display the movie list: ")

            pattern_year = re.compile('19[0-9][0-9]|20[0-1][0-9]')
            if not pattern_year.match(year_raw):
                print("The year introduced %r is not valid!" %year_raw)
                return
   
            choice = input("Press YES if you want to see only a certain number of movies or NO if you want to see all movies: ")


            if not os.path.isfile(os.path.join(os.getcwd(), download_folder, "title.basics.tsv")):
                read_imdbfiles(baseURL, download_folder, endURL = "title.basics.tsv.gz" )
            if not os.path.isfile(os.path.join(os.getcwd(), download_folder, "title.rating.tsv")):
                read_imdbfiles(baseURL, download_folder, endURL = "title.rating.tsv.gz")

            myfile_y = os.path.join(os.getcwd(), download_folder, "title.basics.tsv")
            myfile_r = os.path.join(os.getcwd(), download_folder, "title.rating.tsv")
            all_names_y = pd.read_csv(myfile_y,sep='\t',low_memory=False)
            all_names_r = pd.read_csv(myfile_r,sep='\t',low_memory=False)

            all_names_y = all_names_y.astype(str)
            all_names_r = all_names_r.astype(str)

            searchfor = ['short', 'movie', 'tvMovie', 'tvSeries']   
            id_films_y = (all_names_y.loc[(all_names_y['titleType'].str.contains('|'.join(searchfor))) & (all_names_y['startYear'].str.contains(year_raw))])
  
            if id_films_y.empty == True:
                print("There are no movies for the specified year!")
                break

            ratings = all_names_r[['tconst', 'averageRating']].copy()


            id_film = pd.merge(id_films_y, ratings, how = 'inner', on = 'tconst')

            #print(id_film.columns.tolist())

            id_film_2 = id_film.sort_values(by=['averageRating'], ascending=False)
            
            #list_to_del = [all_names_y, all_names_r, id_films_y, ratings, id_film, id_film_2]
            #del list_to_del
            if choice.upper() == 'YES':
                top_x = input("How many recommendations do you want to see based on the highest rating?: ")   
                try:                        
                    display_year_based_search(id_film_2,top_x)
                except ValueError:
                    print("The number %r is not an integer. Try again!" %top_x)
            elif choice.upper() == 'NO': 
                display_year_based_search_all(id_film_2, download_folder)  
            else:
                print("You've entered wrong values %r!" %choice)
                return  
        else:
            print("Incorrect command!") 
            return     
if __name__ == "__main__":
    main()

